import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesPymesComponent } from './articles-pymes.component';

describe('ArticlesPymesComponent', () => {
  let component: ArticlesPymesComponent;
  let fixture: ComponentFixture<ArticlesPymesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticlesPymesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesPymesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
