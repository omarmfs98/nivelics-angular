import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-articles-pymes',
  templateUrl: './articles-pymes.component.html',
  styleUrls: ['./articles-pymes.component.scss']
})
export class ArticlesPymesComponent implements OnInit {

  @Input()
  articles: object[];

  constructor() {
    this.articles = [];
  }

  ngOnInit(): void {
  }

}
