import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {

  fields;
  has_error: boolean;
  sended: boolean;

  constructor(private formBuilder: FormBuilder) {
    this.fields = this.formBuilder.group({
      first_name: '',
      last_name: '',
      message: ''
    });
    this.sended = false;
    this.has_error = false;
  }

  onSubmit() {
    if (this.fields.valid) {
      this.has_error = false;
      this.sended = true;
    } else {
      this.has_error = true;
    }
  }

  ngOnInit(): void {
  }
}
