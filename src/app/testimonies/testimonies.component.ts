import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-testimonies',
  templateUrl: './testimonies.component.html',
  styleUrls: ['./testimonies.component.scss']
})
export class TestimoniesComponent implements OnInit {

  @Input()
  testimonies: object[];

  constructor() {
    this.testimonies = [];
  }

  ngOnInit(): void {
  }

}
