import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})

export class CategoriesComponent implements OnInit {

  @Input()
  categories: object[];

  constructor(private sanitizer: DomSanitizer) {
    this.categories = [];
  }

  ngOnInit(): void {
    console.log()
    this.categories.forEach(element => {
      element['summary'] = this.sanitizer.bypassSecurityTrustHtml(element['summary'])
      element['short_description'] = this.sanitizer.bypassSecurityTrustHtml(element['short_description'])
    });
  }
}
