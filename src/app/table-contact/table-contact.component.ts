import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-table-contact',
  templateUrl: './table-contact.component.html',
  styleUrls: ['./table-contact.component.scss']
})
export class TableContactComponent implements OnInit {

  @Input()
  form: object;

  constructor() {
    this.form = {};
  }

  ngOnInit(): void {
  }

}
