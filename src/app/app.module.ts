import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderAppComponent } from './header-app/header-app.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { ArticlesPymesComponent } from './articles-pymes/articles-pymes.component';
import { CategoriesComponent } from './categories/categories.component';
import { TestimoniesComponent } from './testimonies/testimonies.component';
import { TableContactComponent } from './table-contact/table-contact.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderAppComponent,
    ContactFormComponent,
    ArticlesPymesComponent,
    CategoriesComponent,
    TestimoniesComponent,
    TableContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
