import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'nivelics-angular';
  website: object;

  constructor() {
    this.website = {
      title_ppal: ""
    };
  }

  ngOnInit(): void {
    fetch("https://d2rpzhocglww2h.cloudfront.net/test/test.json")
      .then(res => res.json())
      .then(res => {
        this.website = res.result;
      });
  }
}
