const plugin = require('tailwindcss/plugin');

module.exports = {
  purge: [],
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [
    plugin(function ({ addBase, config }) {
      addBase({
        'body': { backgroundColor: config('theme.colors.gray.100') },
      })
    }),
  ],
  future: {
    removeDeprecatedGapUtilities: true
  }
}